const passport = require('passport');
const localStrategy = require('passport-local').Strategy
const {
  user
} = require('../../models');
const bcrypt = require('bcrypt');
const JWTstrategy = require('passport-jwt').Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

passport.use(
  'signup',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password',
      passReqToCallback: true
    },
    async (req, email, password, done) => {
      user.create({
        name: req.body.name,
        gender: req.body.gender.toLowerCase(),
        email: email,
        password: bcrypt.hashSync(password, 12)
      }).then(result => {
        return done(null, result, {
          message: 'User created!'
        })
      }).catch(err => {
        if (err) {
          if (err.code === 11000 && err.keyValue.email) {
            return done(null, false, {
              message: 'Email already used!'
            })
          }
          return done(null, false, {
            message: 'User failed to created!'
          })
        }
      })
    }
  )
)

passport.use(
  'login',
  new localStrategy({
      'usernameField': 'email',
      'passwordField': 'password'
    },
    async (email, password, done) => {
      const userLogin = await user.findOne({
          email: email
        })
      if (!userLogin) {
        return done(null, false, {
          message: 'User not found!'
        })
      }
      const validate = await bcrypt.compare(password, userLogin.password);

      if (!validate) {
        return done(null, false, {
          message: 'Wrong password!'
        })
      }
      return done(null, userLogin, {
        message: 'Login success!'
      })
    }
  )
)

passport.use(
  'google',
  new GoogleStrategy({
      clientID: "749458341557-lv6pbaekfmc6q2m1fvtcerke058iqjvk.apps.googleusercontent.com",
      clientSecret: "Aagqw7SI3SHalgSB0puYCYvt",
      callbackURL: process.env.GOOGLE_CALLBACK_URL||"http://localhost:3000/auth/google/callback"
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        let userLogin = await user.findOne({
          email: profile.emails[0].value
        })
        if (!userLogin) {
          userLogin = await user.create({
            name: profile.displayName,
            email: profile.emails[0].value,
            password: bcrypt.hashSync("profile" + profile.name.givenName, 12)
          });
        }
        return done(null, userLogin)
      } catch (error) {
        return done(null, false)
      }
    }
  )
);

passport.use(
  'user',
  new JWTstrategy({
      secretOrKey: 'hashPassword',
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      passReqToCallback: true
    },
    async (req, token, done) => {
      const userLogin = await user.findOne({
        _id: token.user._id
      })
      if (userLogin && userLogin.role.includes('user')) {
        return done(null, token.user)
      } else if (!userLogin) {
        return done(null, false, {
          message: 'User not found!'
        })
      }
      return done(null, false)
    }
  )
)

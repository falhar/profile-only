const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator')
const {
  user
} = require('../../models');

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something

const uploadDir = '/img/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: "./public" + uploadDir, // make images upload to /public/img/
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {
  //Validation check when user signup
  signup: [
    check('name').notEmpty().withMessage('name cant null').matches(/^[a-zA-Z\ \']{3,32}$/)
    .withMessage('invalid name value'),
    check('gender').notEmpty().withMessage('gender cant null').custom(value => {
      if (value.toLowerCase()=='male') {
        return true
      } else if (value.toLowerCase()=='female') {
        return true
      } else {
        throw new Error("input 'male' or 'female'")
      }
    }),
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail().withMessage('email must be email address'),
    check('password').notEmpty().withMessage('password cant null').isLength({
      min: 6,
      max: 32
    }).withMessage('password must have 6 to 32 characters'),
    check('passwordConfirmation')
    .notEmpty().withMessage('password confirmation cant null').custom((value, {
      req
    }) => value === req.body.password).withMessage('wrong password confirmation'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  //Validation check when user login
  login: [
    check('email').notEmpty().withMessage('email cant null').normalizeEmail({
      gmail_remove_dots: false
    }).isEmail().withMessage('input must be email address'),
    check('password').notEmpty().withMessage('password cant null'),
    (req, res, next) => {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next()
    }
  ],
  //Validation check when user update profile
  update: [
    upload.single('image'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.mimetype.startsWith('image')) {
        return true
      } else {
        return false
      }
    }).withMessage('file upload must be images file'),
    check('image').custom((value, {
      req
    }) => {
      if (req.file === undefined) {
        return true
      } else if (req.file.size > 1 * 1024 * 1024) {
        return false
      } else {
        return true
      }
    }).withMessage('file size max 1mb'),
    check('name').notEmpty().withMessage('name cant null').matches(/^[a-zA-Z\ ]{3,32}$/)
    .withMessage('invalid name value'),
    check('gender').notEmpty().withMessage('gender cant null').custom(value => {
      if (value.toLowerCase()=='male') {
        return true
      } else if (value.toLowerCase()=='female') {
        return true
      } else {
        throw new Error("input 'male' or 'female'")
      }
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ]
}

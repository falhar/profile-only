const express = require('express');
const passport = require('passport');
const router = express.Router();
const auth = require('../middlewares/auth');
const userValidator = require('../middlewares/validators/userValidator');
const UserController = require('../controllers/userController');

//Register
router.post('/signup', [userValidator.signup, function(req, res, next) {
  passport.authenticate('signup', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.signup(user, req, res, next);
  })(req, res, next);
}]);

//Login
router.post('/login', [userValidator.login, function(req, res, next) {
  passport.authenticate('login', {
    session: false
  }, function(err, user, info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status: 'Error',
        message: info.message
      });
      return;
    }
    UserController.login(user, req, res, next);
  })(req, res, next);
}]);

//Google Oauth
router.get('/auth/google', passport.authenticate('google', {session:false, scope: ['profile', 'email'] }));

//Google error
router.get('/failed', UserController.failed)

//Google Oauth Callback
router.get('/auth/google/callback', passport.authenticate('google', {session:false, failureRedirect: '/failed' }), UserController.loginGoogle);

//Show Our Profile
router.get('/profile', [passport.authenticate('user', {
  session: false
})], UserController.getOne)

//Update Profile
router.put('/profile/update', [userValidator.update, passport.authenticate('user', {
  session: false
})], UserController.update)


module.exports = router

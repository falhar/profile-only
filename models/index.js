const mongoose = require('mongoose');

const uri = process.env.MONGODB_URI || "mongodb+srv://firman:mongofirman123@sample-data.wtsd3.mongodb.net/profile?retryWrites=true&w=majority"

mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
});

const user = require('./user')


module.exports = {
  user
}

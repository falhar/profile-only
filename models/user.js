const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    default:null
  },
  email: {
    type: String,
    required: true,
    index:{unique:true}
  },
  gender: {
    type:String,
    required:false,
    default:null
  },
  image: {
    type: String,
    default: 'f657f83d5aa4a1747639bec0d017fd97.png',
    required: false
  },
  password: {
    type: String,
    require: true
  },
  role: {
    type: String,
    default:'user'
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false
});

//Add path to the image filename
UserSchema.path('image').get((v)=>{
  return '/img/'+v
})

UserSchema.set('toJSON', {getters:true})

UserSchema.plugin(mongoose_delete, { overrideMethods: 'all' });

module.exports = user = mongoose.model('user', UserSchema, 'user');

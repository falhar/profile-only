const {
  user
} = require('../models');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt')

class UserController {
  //Control Output when user signup
  async signup(user, req, res) {
    const body = {
      _id:user._id
    }
    const token = jwt.sign({
      user:body
    }, 'hashPassword')
    res.status(200).json({
      message:"Sign up success!",
      token:token
    })
  }

  //Callback if login via google failed
  async failed(req, res) {
    try {
      console.log(res);
      res.status(500).json({
        message: "Internal Server Error"
      })
    } catch (e) {
      res.status(500), json({
        message: "Internal Server Error",
        error: e
      })
    }
  }

  //Control Output when user login
  async login(user, req, res) {
    const body = {
      _id:user.id
    }
    const token = jwt.sign({
      user:body
    }, 'hashPassword')
    res.status(200).json({
      message:"Login success!",
      token:token
    })
  }

  //Control Output when user login via google
  async loginGoogle(req, res) {
    const body = {
      _id:req.user._id
    }
    const token = jwt.sign({
      user:body
    }, 'hashPassword')
    res.status(200).json({
      message:"Login success!",
      token:token
    })
  }

  //Control Output when user access profile
  async getOne(req, res) {
    try {
      const result = await user.findOne({
        _id: req.user._id
      })
      res.json({
        status: "Succes get data",
        data: result
      })
    } catch (e) {
      res.status(422).json({
        status: 'error',
        error: e
      })
    }
  }

  //Control Output when user update profile
  async update(req, res) {
    try {
      const prevData = await user.findOne({
        _id: req.user._id
      })
      user.findOneAndUpdate({
        _id: req.user._id
      }, {
        name: req.body.name,
        gender: req.body.gender,
        image: req.file === undefined ? prevData.image.substr(5) : req.file.filename
      }, {
        new: true
      }).then(result => {
        return res.status(200).json({
          status: "Succes updating data",
          data: result
        })
      })
    } catch (e) {
      if (e) {
        if (e.code === 11000 && e.keyValue.email) {
          return res.status(409).json({
            status: 'Error',
            message: 'Email already used!'
          })
        }
      }
    }
  }
}

module.exports = new UserController
